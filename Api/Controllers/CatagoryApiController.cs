﻿using Api.Attribute;
using Api.Models;
using Finalterm.Interface;
using Finalterm.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Api.Controllers
{
    [OrderCustomer]
    public class CatagoryApiController : ApiController
    {
        FinalProjectEntities fp = new FinalProjectEntities();
        IRepository<Catagory> arepo = new CatagoryRepository(new FinalProjectEntities());
        //IRepository<Login> lrepo = new LoginRepository(new FinalProjectEntities());
        //AdminRepository ar = new AdminRepository(new FinalProjectEntities());
        CatagoryRepository cr = new CatagoryRepository(new FinalProjectEntities());

        [Route("api/Catagories")]
        public IHttpActionResult Get()
        {
            var alist = arepo.GetAll().ToList();
            List<Catagory> ad = new List<Catagory>();
            foreach (var item in alist)
            {
                Catagory cr = new Catagory();
                cr.Id = item.Id;
                cr.Name = item.Name;
               

                cr.links.Add(new Links() { Href = "http://localhost:8901/api/Catagories", method = "Get", rel = "Self" });
                cr.links.Add(new Links() { Href = "http://localhost:8901/api/Catagories/ " + cr.Id, method = "Get", rel = "Specific Resource" });
                cr.links.Add(new Links() { Href = "http://localhost:8901/api/Catagories", method = "Post", rel = "Create Resource" });
                cr.links.Add(new Links() { Href = "http://localhost:8901/api/Catagories / " + cr.Id, method = "Put", rel = "Update Resource" });
                cr.links.Add(new Links() { Href = "http://localhost:8901/api/Catagories / " + cr.Id, method = "Delete", rel = "Delete Resource" });
                ad.Add(cr);
            }
            return Ok(ad);
       
        }
        [Route("api/Catagories/{id}", Name = "GetIdBy")]
        public IHttpActionResult Get(string id)
        {
            var alist = arepo.GetAll().ToList().Where(p => p.Id == int.Parse(id)).FirstOrDefault();
            
            if (alist == null)
            {
                return StatusCode(HttpStatusCode.NoContent);
            }
            else
            {
                Catagory a = new Catagory();
                a.Id =alist.Id;
                a.Name = alist.Name;


                a.links.Add(new Links() { Href = "http://localhost:8901/api/Admins", method = "Get", rel = "All Resources" });
                a.links.Add(new Links() { Href = "http://localhost:8901/api/Admins/" + a.Id, method = "Get", rel = "Self" });
                a.links.Add(new Links() { Href = "http://localhost:8901/api/Admins", method = "Post", rel = "Create Resource" });
                a.links.Add(new Links() { Href = "http://localhost:8901/api/Admins/" + a.Id, method = "Put", rel = "Update Resource" });
                a.links.Add(new Links() { Href = "http://localhost:8901/api/Admins/" + a.Id, method = "Delete", rel = "Delete Resource" });
                return Ok(a);
            }
        }

        [Route("api/Catagories")]
        public IHttpActionResult Post(Catagory c)
        {
            if (c == null)
            {
                return StatusCode(HttpStatusCode.NoContent);
            }
            else
            {
                
                arepo.Insert(c);
                c.links.Add(new Links() { Href = "http://localhost:8901/api/Catagories", method = "Get", rel = "All Resource" });
                c.links.Add(new Links() { Href = "http://localhost:8901/api/Catagories/ " + c.Id, method = "Get", rel = "Specific Resource" });
                c.links.Add(new Links() { Href = "http://localhost:8901/api/Catagories", method = "Post", rel = "Self" });
                c.links.Add(new Links() { Href = "http://localhost:8901/api/Catagories / " + c.Id, method = "Put", rel = "Update Resource" });
                c.links.Add(new Links() { Href = "http://localhost:8901/api/Catagories / " + c.Id, method = "Delete", rel = "Delete Resource" });
                return Created(Url.Link("GetIdBy", new { id = c.Id }), c);
            }
        }

        [Route("api/Catagories/{id}")]
        public IHttpActionResult Put([FromUri]string id, [FromBody]Catagory cg)
        {
            var Catagory = arepo.GetById(id);

            if (Catagory == null)
            {
                return StatusCode(HttpStatusCode.NoContent);
            }
            else
            {
                cg.Id = Catagory.Id;
                cg.links.Add(new Links() { Href = "http://localhost:8901/api/Catagories", method = "Get", rel = "All Resource" });
                cg.links.Add(new Links() { Href = "http://localhost:8901/api/Catagories/ " + cg.Id, method = "Get", rel = "Specific Resource" });
                cg.links.Add(new Links() { Href = "http://localhost:8901/api/Catagories", method = "Post", rel = "Create Resource" });
                cg.links.Add(new Links() { Href = "http://localhost:8901/api/Catagories / " + cg.Id, method = "Put", rel = "Slef" });
                cg.links.Add(new Links() { Href = "http://localhost:8901/api/Catagories / " + cg.Id, method = "Delete", rel = "Delete Resource" });
                cr.update(cg);
                return Created(Url.Link("GetIdBy", new { id = cg.Id }), cg);
            }

        }
        [Route("api/Catagories/{id}")]
        public IHttpActionResult Delete(string id)
        {
            var a = arepo.GetById(id);
            arepo.Delete(a);
            return StatusCode(HttpStatusCode.NoContent);
        }


    }

}
