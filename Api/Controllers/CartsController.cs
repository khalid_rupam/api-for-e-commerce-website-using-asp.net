﻿using Api.Attribute;
using Api.Models;
using Finalterm.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Api.Controllers
{
    [RoutePrefix("api/customer/{id}/carts")]
    [OrderCustomer]
    public class CartsController : ApiController
    {
        
        CartsRepository cr = new CartsRepository(new FinalProjectEntities());
        ProductRepostory pr = new ProductRepostory(new FinalProjectEntities());
        [Route("")]

        public IHttpActionResult Get(string id)
        {
            var tempList= cr.GetByIdc(id);
            List<Cart> cartlist = new List<Cart>();
            foreach (var item in tempList)
            {
                Cart temp = new Cart() { C_ID = item.C_ID, Id=item.Id, Price=item.Price, P_Id=item.P_Id, Quantity=item.Quantity, time=item.time };
                temp.links.Add(new Links() { Href = "http://localhost:8901/api/customer/" + temp.C_ID + "/carts", method = "Get", rel = "self" });
                temp.links.Add(new Links() { Href = "http://localhost:8901/api/customer/" + temp.C_ID + "/carts" + temp.Id, method = "Get", rel = "specific Resource" });
                temp.links.Add(new Links() { Href = "http://localhost:8901/api/customer/" + temp.C_ID + "/carts", method = "Post", rel = "Create Resource" });
                temp.links.Add(new Links() { Href = "http://localhost:8901/api/customer/" + temp.C_ID + "/carts" + temp.Id, method = "Delete", rel = "Delete Resource" });
                temp.links.Add(new Links() { Href = "http://localhost:8901/api/customer/" + temp.C_ID + "/carts" + temp.Id + "/products", method = "Delete", rel = "Nested Resource" });
                cartlist.Add(temp);
            }
            return Ok(cartlist);
        }
        [Route("{cid}",Name ="GetOneCart")]
        public IHttpActionResult Get(int cid)
        {
            var cart = cr.GetById(cid.ToString());
            if (cart == null)
            {
                return StatusCode(HttpStatusCode.NoContent);
            }
            else
            {
                Cart temp = new Cart() { C_ID = cart.C_ID, Id = cart.Id, Price = cart.Price, P_Id = cart.P_Id, Quantity = cart.Quantity, time = cart.time };
                temp.links.Add(new Links() { Href = "http://localhost:8901/api/customer/" + temp.C_ID + "/carts", method = "Get", rel = "All Resources" });
                temp.links.Add(new Links() { Href = "http://localhost:8901/api/customer/" + temp.C_ID + "/carts" + temp.Id, method = "Get", rel = "self" });
                temp.links.Add(new Links() { Href = "http://localhost:8901/api/customer/" + temp.C_ID + "/carts", method = "Post", rel = "Create Resource" });
                temp.links.Add(new Links() { Href = "http://localhost:8901/api/customer/" + temp.C_ID + "/carts" + temp.Id, method = "Delete", rel = "Delete Resource" });
                temp.links.Add(new Links() { Href = "http://localhost:8901/api/customer/" + temp.C_ID + "/carts" + temp.Id + "/products", method = "Delete", rel = "Nested Resource" });
                return Ok(temp);
            }
            
        }
        [Route("{cid}")]

        public IHttpActionResult Delete(int cid)
        {
            var cart = cr.GetById(cid.ToString());

            cr.Delete(cart);

            return StatusCode(HttpStatusCode.NoContent);
        }
        [Route("")]
        public IHttpActionResult Post(string id,Cart cart)
        {
            if (cart==null)
            {
                return StatusCode(HttpStatusCode.NoContent);
            }
            else
            {
                cart.C_ID = id;
                cr.Insert(cart);
                Cart temp = new Cart() { C_ID = cart.C_ID, Id = cart.Id, Price = cart.Price, P_Id = cart.P_Id, Quantity = cart.Quantity, time = cart.time };
                temp.links.Add(new Links() { Href = "http://localhost:8901/api/customer/" + temp.C_ID + "/carts", method = "Get", rel = "All Resources" });
                temp.links.Add(new Links() { Href = "http://localhost:8901/api/customer/" + temp.C_ID + "/carts" + temp.Id, method = "Get", rel = "Specific Resource" });
                temp.links.Add(new Links() { Href = "http://localhost:8901/api/customer/" + temp.C_ID + "/carts", method = "Post", rel = "Self" });
                temp.links.Add(new Links() { Href = "http://localhost:8901/api/customer/" + temp.C_ID + "/carts" + temp.Id, method = "Delete", rel = "Delete Resource" });
                temp.links.Add(new Links() { Href = "http://localhost:8901/api/customer/" + temp.C_ID + "/carts" + temp.Id + "/products", method = "Delete", rel = "Nested Resource" });

                return Created(Url.Link("GetOneCart", new { cid = temp.Id }), temp);
                //return Ok(temp);

            }
        }
        [Route("{cid}/product")]
        public IHttpActionResult GetProductssOfCarts(string id,int cid)
        {
            var cart = cr.GetById(cid.ToString());
            if (cart == null)
            {
                return StatusCode(HttpStatusCode.NoContent);
            }
            else
            {
                var product = pr.GetById(cart.P_Id.ToString());
                Product temp = new Product() { Buy_Price = product.Buy_Price, Catagory_Id = product.Catagory_Id, Description = product.Description, Id = product.Id, image = product.image, Name = product.Name, Quantity = product.Quantity, Rating = product.Rating, Sell_Price = product.Sell_Price, Unit_Id = product.Unit_Id, Brand = product.Brand };
                temp.links.Add(new Links() { Href = "http://localhost:8901/api/customer/" + id + "/carts", method = "Get", rel = "All Resources" });
                temp.links.Add(new Links() { Href = "http://localhost:8901/api/customer/" + id + "/carts" + temp.Id, method = "Get", rel = "self" });
                temp.links.Add(new Links() { Href = "http://localhost:8901/api/customer/" + id + "/carts", method = "Post", rel = "Create Resource" });
                temp.links.Add(new Links() { Href = "http://localhost:8901/api/customer/" + id + "/carts" + temp.Id, method = "Delete", rel = "Delete Resource" });
                temp.links.Add(new Links() { Href = "http://localhost:8901/api/customer/" + id + "/carts" + temp.Id + "/products", method = "Delete", rel = "Nested Resource" });

                return Ok(temp);
            } 
        }
        [Route("{cid}")]
        public IHttpActionResult Put([FromUri]string id, [FromUri]int cid,[FromBody]Cart c)
        {
            var cart = cr.GetById(cid.ToString());
            if (cart == null)
            {
                return StatusCode(HttpStatusCode.NoContent);
            }
            else
            {
                CartsRepository cr = new CartsRepository(new FinalProjectEntities());
                c.C_ID = id;
                c.Id = cid;
                cr.update(c);
                c.links.Add(new Links() { Href = "http://localhost:8901/api/customer/" + c.C_ID + "/carts", method = "Get", rel = "All Resources" });
                c.links.Add(new Links() { Href = "http://localhost:8901/api/customer/" + c.C_ID + "/carts" + c.Id, method = "Get", rel = "Specific Resource" });
                c.links.Add(new Links() { Href = "http://localhost:8901/api/customer/" + c.C_ID + "/carts", method = "Post", rel = "Create Resource" });
                c.links.Add(new Links() { Href = "http://localhost:8901/api/customer/" + c.C_ID + "/carts" + c.Id, method = "Put", rel = "Self" });
                c.links.Add(new Links() { Href = "http://localhost:8901/api/customer/" + c.C_ID + "/carts" + c.Id, method = "Delete", rel = "Delete Resource" });
                c.links.Add(new Links() { Href = "http://localhost:8901/api/customer/" + c.C_ID + "/carts" + c.Id + "/products", method = "Get", rel = "Nested Resource" });
                return Ok(c);
            }
        }
    }

}
