﻿using Api.Attribute;
using Api.Models;
using Api.Repository;
using Finalterm.Interface;
using Finalterm.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Api.Controllers
{
    [RoutePrefix("api/Products")]
    [OrderCustomer]
    public class ProductController : ApiController
    {
        IRepository<Product> Prepo = new ProductRepostory(new FinalProjectEntities());
        IRepository<Catagory> crepo = new CatagoryRepository(new FinalProjectEntities());
        IRepository<Unit> urepo = new UnitRepository(new FinalProjectEntities());
        [Route("")]
        public IHttpActionResult Get()
        {
            var plist = Prepo.GetAll().ToList();
            List<Product> product = new List<Product>();
            foreach (var item in plist)
            {
                Product p = new Product();
                p.Brand = item.Brand;
                p.Sell_Price = item.Sell_Price;
                p.Catagory_Id = item.Catagory_Id;
                p.Description = item.Description;
                p.Id = item.Id;
                p.image = item.image;
                p.Name = item.Name;
                p.Rating = item.Rating;
                p.Unit_Id = item.Unit_Id; 
                p.links.Add(new Links() { Href = "http://localhost:8901/api/Products", method = "Get", rel = "Self" });
                p.links.Add(new Links() { Href = "http://localhost:8901/api/Products"+p.Id, method = "Get", rel = "Specific Resource" });
                p.links.Add(new Links() { Href = "http://localhost:8901/api/Products", method = "Post", rel = "Resource create" });
                p.links.Add(new Links() { Href = "http://localhost:8901/api/Products"+p.Id, method = "Put", rel = "Resource Update" });
                p.links.Add(new Links() { Href = "http://localhost:8901/api/Products"+p.Id, method = "Delete", rel = "Resource Delete" });

                product.Add(p);
            }
            return Ok(product);
        }
        [Route("{id}",Name ="GetById")]
        public IHttpActionResult Get(string id)
        {
            var product = Prepo.GetById(id);
            if (product==null)
            {
                return StatusCode(HttpStatusCode.NoContent);
            }
            else
            {
                Product p = new Product();
                p.Brand = product.Brand;
                p.Sell_Price = product.Sell_Price;
                p.Catagory_Id = product.Catagory_Id;
                p.Description = product.Description;
                p.Id = product.Id;
                p.Buy_Price = product.Buy_Price;
                p.image = product.image;
                p.Name = product.Name;
                p.Rating = product.Rating;
                p.Unit_Id = product.Unit_Id;
                p.links.Add(new Links() { Href = "http://localhost:8901/api/Products", method = "Get", rel = "All Resource" });
                p.links.Add(new Links() { Href = "http://localhost:8901/api/Products" + p.Id, method = "Get", rel = "Self" });
                p.links.Add(new Links() { Href = "http://localhost:8901/api/Products", method = "Post", rel = "Resource create" });
                p.links.Add(new Links() { Href = "http://localhost:8901/api/Products" + p.Id, method = "Put", rel = "Resource Update" });
                p.links.Add(new Links() { Href = "http://localhost:8901/api/Products" + p.Id, method = "Delete", rel = "Resource Delete" });

                return Ok(p);
            } 
        }
        [Route("{id}")]
        public IHttpActionResult Put([FromUri]string id,[FromBody]Product p)
        {
            var a = Prepo.GetById(id);
            if(a==null)
            {
                return StatusCode(HttpStatusCode.NoContent);
            }
            ProductRepostory pr = new ProductRepostory(new FinalProjectEntities());

            p.Id = int.Parse(id);
            pr.update(p);
            p.links.Add(new Links() { Href = "http://localhost:8901/api/Products", method = "Get", rel = "All Resource" });
            p.links.Add(new Links() { Href = "http://localhost:8901/api/Products" + p.Id, method = "Get", rel = "Specific Resource" });
            p.links.Add(new Links() { Href = "http://localhost:8901/api/Products", method = "Post", rel = "Resource create" });
            p.links.Add(new Links() { Href = "http://localhost:8901/api/Products" + p.Id, method = "Put", rel = "Self" });
            p.links.Add(new Links() { Href = "http://localhost:8901/api/Products" + p.Id, method = "Delete", rel = "Resource Delete" });
            return Ok(p);
        }
        [Route("")]
        public IHttpActionResult Post( [FromBody]Product p1)
        {
            Prepo.Insert(p1);
            Product p = new Product();
            p.Brand = p1.Brand;
            p.Buy_Price = p1.Buy_Price;
            p.Sell_Price = p1.Sell_Price;
            p.Catagory_Id = p1.Catagory_Id;
            p.Description = p1.Description;
            p.Id = p1.Id;
            p.image = p1.image;
            p.Name = p1.Name;
            p.Rating = p1.Rating;
            p.Unit_Id = p1.Unit_Id;
            p.links.Add(new Links() { Href = "http://localhost:8901/api/Products", method = "Get", rel = "All Resource" });
            p.links.Add(new Links() { Href = "http://localhost:8901/api/Products" + p.Id, method = "Get", rel = "Self" });
            p.links.Add(new Links() { Href = "http://localhost:8901/api/Products", method = "Post", rel = "Resource create" });
            p.links.Add(new Links() { Href = "http://localhost:8901/api/Products" + p.Id, method = "Put", rel = "Resource Update" });
            p.links.Add(new Links() { Href = "http://localhost:8901/api/Products" + p.Id, method = "Delete", rel = "Resource Delete" });
            return Created(Url.Link("GetById", new { id = p.Id }), p);
        }
        [Route("{id}")]
        public IHttpActionResult Delete(string id)
        {
            var l = Prepo.GetById(id);
            if (l==null)
            {
                return StatusCode(HttpStatusCode.NoContent);
            }
            else
            {
                Prepo.Delete(l);
                return StatusCode(HttpStatusCode.NoContent);
            } 
        }
        [Route("{id}/Catagories")]
        public IHttpActionResult GetByCatagory(string id)
        {
            var l = Prepo.GetById(id);
            if (l==null)
            {
                return StatusCode(HttpStatusCode.NoContent);
            }
            else
            {
                var a = crepo.GetById(l.Catagory_Id.ToString());
                if (a==null)
                {
                    return StatusCode(HttpStatusCode.NoContent);
                }
                else
                {
                    Catagory c = new Catagory();
                    c.Id = a.Id;
                    c.Name = a.Name;
                    c.links.Add(new Links() { Href = "http://localhost:8901/api/Products/" + id + "/Catagories", method = "Get", rel = "Self" });
                    //c.links.Add(new Links() { Href = "http://localhost:8901/api/Products/" + id + "/Catagories", method = "Put", rel = "Update Specific Resource" });
                    return Ok(c);
                }
            }
        }
        [Route("{id}/Units")]
        public IHttpActionResult GetByUnits(string id)
        {
            var l = Prepo.GetById(id);
            if (l == null)
            {
                return StatusCode(HttpStatusCode.NoContent);
            }
            else
            {
                var a = urepo.GetById(l.Unit_Id.ToString());
                if (a == null)
                {
                    return StatusCode(HttpStatusCode.NoContent);
                }
                else
                {
                    Unit u = new Unit();
                    u.Id = a.Id;
                    u.Name = l.Name;
                    u.links.Add(new Links() { Href = "http://localhost:8901/api/Products/" + id + "/Units", method = "Get", rel = "Self" });
                    //c.links.Add(new Links() { Href = "http://localhost:8901/api/Products/" + id + "/Catagories", method = "Put", rel = "Update Specific Resource" });
                    return Ok(u);
                }
            }
        }
    }
}
