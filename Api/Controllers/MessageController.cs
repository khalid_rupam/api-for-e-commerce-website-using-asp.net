﻿using Api.Attribute;
using Api.Models;
using Finalterm.Interface;
using Finalterm.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Api.Controllers
{
    [RoutePrefix("api")]
    [OrderCustomer]
    public class MessageController : ApiController
    {
        IRepository<Message> mrepo = new MessageRepository(new FinalProjectEntities());
        IRepository<Customer> crepo = new CustomerRepository(new FinalProjectEntities());
        [Route("Messages")]
        public IHttpActionResult Get()
        {
            var list = mrepo.GetAll().ToList();
            List<Message> mlist = new List<Message>();

            foreach (var item in list)
            {
                Message m = new Message();
                m.Id = item.Id;
                m.Sender_Id = item.Sender_Id;
                m.Status = item.Status;
                m.Messages = item.Messages;
                m.links.Add(new Links() { Href = "http://localhost:8901/api/Messages", method = "Get", rel = "Self" });
                m.links.Add(new Links() { Href = "http://localhost:8901/api/Messages/"+m.Id, method = "Get", rel = "Specific Resource" });
               // m.links.Add(new Links() { Href = "http://localhost:8901/api/Messages", method = "Post", rel = "Create Resource" });
                m.links.Add(new Links() { Href = "http://localhost:8901/api/Messages/"+m.Id, method = "Put", rel = "Update Resource" });
                m.links.Add(new Links() { Href = "http://localhost:8901/api/Messages/"+m.Id, method = "Delete", rel = "Delete resource" });
                mlist.Add(m);

            }
            return Ok(mlist);
        }
        [Route("Messages/{id}")]
        public IHttpActionResult Get(string id)
        {
            var a = mrepo.GetById(id);

            if (a==null)
            {
                return StatusCode(HttpStatusCode.NoContent);
            }
            else
            {
                Message m = new Message();
                m.Id = a.Id;
                m.Sender_Id = a.Sender_Id;
                m.Messages = a.Messages;
                m.Status = a.Status;
                m.links.Add(new Links() { Href = "http://localhost:8901/api/Messages", method = "Get", rel = "All Resource" });
                m.links.Add(new Links() { Href = "http://localhost:8901/api/Messages/" + m.Id, method = "Get", rel = "Self" });
                //m.links.Add(new Links() { Href = "http://localhost:8901/api/Messages", method = "Post", rel = "Create Resource" });
                m.links.Add(new Links() { Href = "http://localhost:8901/api/Messages/" + m.Id, method = "Put", rel = "Update Resource" });
                m.links.Add(new Links() { Href = "http://localhost:8901/api/Messages/" + m.Id, method = "Delete", rel = "Delete resource" });
                return Ok(m);
            }
        }
        [Route("Messages/{id}")]
        public IHttpActionResult Put([FromUri]string id,[FromBody]Message m)
        {
            var a = mrepo.GetById(id);

            if (a==null)
            {
                return StatusCode(HttpStatusCode.NoContent);
            }
            else
            {
                MessageRepository mr = new MessageRepository(new FinalProjectEntities());
                Message m1 = new Message();
                m1.Id = a.Id;
                m1.Messages = m.Messages;
                m1.Sender_Id = m.Sender_Id;
                m1.Status = m.Status;
                
                mr.update(m1);
                m1.links.Add(new Links() { Href = "http://localhost:8901/api/Messages", method = "Get", rel = "All Resource" });
                m1.links.Add(new Links() { Href = "http://localhost:8901/api/Messages/" + m.Id, method = "Get", rel = "Specific Resource" });
                //m.links.Add(new Links() { Href = "http://localhost:8901/api/Messages", method = "Post", rel = "Create Resource" });
                m1.links.Add(new Links() { Href = "http://localhost:8901/api/Messages/" + m.Id, method = "Put", rel = "Self" });
                m1.links.Add(new Links() { Href = "http://localhost:8901/api/Messages/" + m.Id, method = "Delete", rel = "Delete resource" });

                return Ok(m1);
            }
        }
        [Route("Messages/{id}")]
        public IHttpActionResult Delete([FromUri]string id)
        {
            var a = mrepo.GetById(id);
            if (a == null)
            {
                return StatusCode(HttpStatusCode.NoContent);
            }
            else
            {
                mrepo.Delete(a);
                return StatusCode(HttpStatusCode.NoContent);
            }
        }
        [Route("Customers/{id}/Messages")]
        public IHttpActionResult GetByCustomer(string id)
        {
            var a= mrepo.GetAll().ToList();
            List<Message> ml = new List<Message>();
            foreach (var item in a)
            {
                Message m = new Message();
                if (item.Sender_Id==id)
                {
                    m.Id = item.Id;
                    m.Messages = item.Messages;
                    m.Sender_Id = item.Sender_Id;
                    m.Status = item.Status;
                    m.links.Add(new Links() { Href = "http://localhost:8901/api/Customers/"+m.Sender_Id+"/Messages", method = "Get", rel = "Self" });
                    m.links.Add(new Links() { Href = "http://localhost:8901/api/Customers/" + m.Sender_Id + "/Messages" + m.Id, method = "Get", rel = "Specific Resource" });
                    m.links.Add(new Links() { Href = "http://localhost:8901/api/Customers/"+m.Sender_Id+"/Messages", method = "Post", rel = "Create Resource" });
                    m.links.Add(new Links() { Href = "http://localhost:8901/api/Customers/" + m.Sender_Id + "/Messages" + m.Id, method = "Put", rel = "Update Resource" });
                    m.links.Add(new Links() { Href = "http://localhost:8901/api/Customers/" + m.Sender_Id + "/Messages" + m.Id, method = "Delete", rel = "Delete resource" });

                    ml.Add(m);
                }
            }
            return Ok(ml);
        }
        [Route("Customers/{id}/Messages/{Mid}",Name = "Getonebycustomer")]
        public IHttpActionResult GetByCustomer(string id,int Mid)
        {
            var a = mrepo.GetAll().ToList();
            List<Message> ml = new List<Message>();
            foreach (var item in a)
            {
                Message m = new Message();
                if (item.Sender_Id == id&&item.Id==Mid)
                {
                    m.Id = item.Id;
                    m.Messages = item.Messages;
                    m.Sender_Id = item.Sender_Id;
                    m.Status = item.Status;
                    m.links.Add(new Links() { Href = "http://localhost:8901/api/Customers/" + m.Sender_Id + "/Messages", method = "Get", rel = "All Resource" });
                    m.links.Add(new Links() { Href = "http://localhost:8901/api/Customers/" + m.Sender_Id + "/Messages" + m.Id, method = "Get", rel = "Self" });
                    m.links.Add(new Links() { Href = "http://localhost:8901/api/Customers/" + m.Sender_Id + "/Messages", method = "Post", rel = "Create Resource" });
                    m.links.Add(new Links() { Href = "http://localhost:8901/api/Customers/" + m.Sender_Id + "/Messages" + m.Id, method = "Put", rel = "Update Resource" });
                    m.links.Add(new Links() { Href = "http://localhost:8901/api/Customers/" + m.Sender_Id + "/Messages" + m.Id, method = "Delete", rel = "Delete resource" });

                    ml.Add(m);
                }
            }
            return Ok(ml);
        }
        [Route("Customers/{id}/Messages")]
        public IHttpActionResult Post([FromUri]string id,[FromBody]Message m)
        {
            var a= crepo.GetById(id);
            if (a==null)
            {
                return StatusCode(HttpStatusCode.NoContent);
            }
            else
            {
                m.Sender_Id = id;
                
                mrepo.Insert(m);
                m.links.Add(new Links() { Href = "http://localhost:8901/api/Customers/" + m.Sender_Id + "/Messages", method = "Get", rel = "All Resource" });
                m.links.Add(new Links() { Href = "http://localhost:8901/api/Customers/" + m.Sender_Id + "/Messages" + m.Id, method = "Get", rel = "Specific Resource" });
                m.links.Add(new Links() { Href = "http://localhost:8901/api/Customers/" + m.Sender_Id + "/Messages", method = "Post", rel = "Self" });
                m.links.Add(new Links() { Href = "http://localhost:8901/api/Customers/" + m.Sender_Id + "/Messages" + m.Id, method = "Put", rel = "Update Resource" });
                m.links.Add(new Links() { Href = "http://localhost:8901/api/Customers/" + m.Sender_Id + "/Messages" + m.Id, method = "Delete", rel = "Delete resource" });

                return Created(Url.Link("Getonebycustomer", new { id = id, Mid=m.Id }), m);
            }
        }
        [Route("Customers/{id}/Messages/{Mid}")]
        public IHttpActionResult Put([FromUri]string id,[FromUri] int Mid, [FromBody]Message m)
        {
            var a = mrepo.GetAll().Where(p=>p.Id==Mid).FirstOrDefault();
            Message m1 = new Message();
            if (a == null)
            {
                return StatusCode(HttpStatusCode.NoContent);
            }
            else
            {
                if(!a.Sender_Id.Equals(id))
                {
                    return StatusCode(HttpStatusCode.NoContent);
                }
                else
                {
                    MessageRepository mr = new MessageRepository(new FinalProjectEntities());
                    
                    m1.Id = Mid;
                    m1.Messages = m.Messages;
                    m1.Sender_Id = id;
                    m1.Status = m.Status;
                    mr.update(m1);
                    m1.links.Add(new Links() { Href = "http://localhost:8901/api/Customers/" + m.Sender_Id + "/Messages", method = "Get", rel = "All Resource" });
                    m1.links.Add(new Links() { Href = "http://localhost:8901/api/Customers/" + m.Sender_Id + "/Messages" + m.Id, method = "Get", rel = "Specific Resource" });
                    m1.links.Add(new Links() { Href = "http://localhost:8901/api/Customers/" + m.Sender_Id + "/Messages", method = "Post", rel = "Create Resource" });
                    m1.links.Add(new Links() { Href = "http://localhost:8901/api/Customers/" + m.Sender_Id + "/Messages" + m.Id, method = "Put", rel = "Self" });
                    m1.links.Add(new Links() { Href = "http://localhost:8901/api/Customers/" + m.Sender_Id + "/Messages" + m.Id, method = "Delete", rel = "Delete resource" });
                }
                return Created(Url.Link("Getonebycustomer", new { id = id, Mid = m1.Id }), m1);
            }
        }
        [Route("Customers/{id}/Messages/{Mid}")]
        public IHttpActionResult Delete([FromUri]string id, [FromUri] int Mid)
        {
            var a = mrepo.GetAll().Where(p => p.Id == Mid).FirstOrDefault();
            Message m1 = new Message();
            if (a == null)
            {
                return StatusCode(HttpStatusCode.NoContent);
            }
            else
            {
                if (!a.Sender_Id.Equals(id))
                {
                    return StatusCode(HttpStatusCode.NoContent);
                }
                else
                {
                    mrepo.Delete(a);
                }
                return StatusCode(HttpStatusCode.NoContent);
            }
        }
    }
}
