﻿using Api.Attribute;
using Api.Models;
using Finalterm.Interface;
using Finalterm.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Api.Controllers
{
    [RoutePrefix("api/Logins")]
    [OrderCustomer]

    public class LoginController : ApiController
    {FinalProjectEntities
         context = new FinalProjectEntities();
        IRepository<Login> papo = new LoginRepository(new FinalProjectEntities());

        [Route("")]
        public IHttpActionResult Get()
        {
            var clist = papo.GetAll().ToList();
            List<Login> cl = new List<Login>();

            foreach (var item in clist)
            {
                Login c = new Login();
                c.ID = item.ID;
                c.Password = item.Password;
                c.Type= item.Type;
                c.Status = item.Status;
                c.Online = item.Online;

                c.links.Add(new Links() { Href = "http://localhost:8901/api/Logins", method = "Get", rel = "Self" });
                c.links.Add(new Links() { Href = "http://localhost:8901/api/Logins/" + c.ID, method = "Get", rel = "Specific Resource" });
                c.links.Add(new Links() { Href = "http://localhost:8901/api/Logins", method = "Post", rel = "Create Resource" });
                c.links.Add(new Links() { Href = "http://localhost:8901/api/Logins/" + c.ID, method = "Delete", rel = "Delete Resource" });
                cl.Add(c);
            }
            return Ok(cl);

        }
        [Route("{id}", Name = "GetProById")]
        public IHttpActionResult Get(string id)
        {

            var clist = papo.GetAll().ToList().Where(p => p.ID == id).FirstOrDefault();

            if (clist == null)
            {
                return StatusCode(HttpStatusCode.NoContent);
            }
            else
            {
                Login c = new Login();
                c.ID = clist.ID;
                c.Password = clist.Password;
                c.Status= clist.Status;
                c.Online = clist.Online;



            c.links.Add(new Links() { Href = "http://localhost:8901/api/Logins", method = "Get", rel = "All Resources" });
                c.links.Add(new Links() { Href = "http://localhost:8901/api/Logins", method = "Post", rel = "Create Resource" });
                c.links.Add(new Links() { Href = "http://localhost:8901/api/Logins/" + c.ID, method = "Get", rel = "Self" });
                c.links.Add(new Links() { Href = "http://localhost:8901/api/Logins/" + c.ID, method = "Delete", rel = "Delete Resource" });
                return Ok(c);
            }

        }


        [Route("{id}")]
        public IHttpActionResult Delete([FromUri]int id)
        {
            var c = papo.GetById(id.ToString());
            papo.Delete(c);
            return StatusCode(HttpStatusCode.NoContent);
  
        }
        [Route("Customers")]
        public IHttpActionResult Post(Login product)
        {
            Random rf=new Random();
            Login c = new Login();
            c.ID = "C" + rf.Next(999);
            c.Online = product.Online;
            c.Password = rf.Next(999).ToString();
            c.Status = 0;
            c.Type = 0;
            papo.Insert(c);
            c.links.Add(new Links() { Href = "http://localhost:8901/api/Logins", method = "Get", rel = "All Resources" });
            c.links.Add(new Links() { Href = "http://localhost:8901/api/Logins", method = "Post", rel = "Self" });
            c.links.Add(new Links() { Href = "http://localhost:8901/api/Logins/" + c.ID, method = "Get", rel = "Specific Resource" });
            c.links.Add(new Links() { Href = "http://localhost:8901/api/Logins/" + c.ID, method = "Delete", rel = "Delete Resource" });
            return Created(Url.Link("GetoneComm", new { id = c.ID }),c );
        }


    }
}
