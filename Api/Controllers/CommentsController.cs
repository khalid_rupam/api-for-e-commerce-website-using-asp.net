﻿using Api.Attribute;
using Api.Models;
using Finalterm.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Api.Controllers
{
    [RoutePrefix("api/products/{id}/comments")]
    [OrderCustomer]
    public class CommentsController : ApiController
    {
        ProductRepostory pr = new ProductRepostory(new FinalProjectEntities());
        CommentRepository cr = new CommentRepository(new FinalProjectEntities());
        CustomerRepository csr = new CustomerRepository(new FinalProjectEntities());
        [Route("")]
        public IHttpActionResult Get(int id)
        {
            var commentList = cr.GetByProduct(id);
            List<Comment> templist = new List<Comment>();

            foreach (var item in commentList)
            {
                Comment temp = new Comment { Annonyms = item.Annonyms, C_Id = item.C_Id, ID = item.ID, P_Id = item.P_Id, rating = item.rating, time = item.time, Comment1 = item.Comment1 };

                temp.links.Add(new Links() { Href = "http://localhost:8901/api/products/" + temp.P_Id + "/comments", method = "Get", rel = "self" });
                temp.links.Add(new Links() { Href = "http://localhost:8901/api/products/" + temp.P_Id + "/comments/" + temp.ID, method = "Get", rel = "specific Resource" });
                temp.links.Add(new Links() { Href = "http://localhost:8901/api/products/" + temp.P_Id + "/comments", method = "Post", rel = "Create Resource" });
                temp.links.Add(new Links() { Href = "http://localhost:8901/api/products/" + temp.P_Id + "/comments/" + temp.ID, method = "Delete", rel = "Delete Resource" });
                temp.links.Add(new Links() { Href = "http://localhost:8901/api/products/" + temp.P_Id + "/comments/" + temp.ID, method = "put", rel = "Update Resource" });
                templist.Add(temp);
            }
            return Ok(templist);
        }
        [Route("{cmid}", Name = "GetOneComment")]
        public IHttpActionResult Get(int cmid, int id)
        {
            var comment = cr.GetById(cmid.ToString());
            if (comment == null)
            {
                return StatusCode(HttpStatusCode.NoContent);
            }
            else
            {
                Comment temp = new Comment { Annonyms = comment.Annonyms, C_Id = comment.C_Id, ID = comment.ID, P_Id = comment.P_Id, rating = comment.rating, time = comment.time, Comment1 = comment.Comment1 };
                temp.links.Add(new Links() { Href = "http://localhost:8901/api/products/" + temp.P_Id + "/comments", method = "Get", rel = "All resource" });
                temp.links.Add(new Links() { Href = "http://localhost:8901/api/products/" + temp.P_Id + "/comments/" + temp.ID, method = "Get", rel = "self" });
                temp.links.Add(new Links() { Href = "http://localhost:8901/api/products/" + temp.P_Id + "/comments", method = "Post", rel = "Create Resource" });
                temp.links.Add(new Links() { Href = "http://localhost:8901/api/products/" + temp.P_Id + "/comments/" + temp.ID, method = "Delete", rel = "Delete Resource" });
                temp.links.Add(new Links() { Href = "http://localhost:8901/api/products/" + temp.P_Id + "/comments/" + temp.ID, method = "put", rel = "Update Resource" });
                return Ok(temp);
            }
        }
        [Route("")]
        public IHttpActionResult Post(Comment comment)
        {
            if (comment == null)
            {
                return StatusCode(HttpStatusCode.NoContent);
            }
            else
            {
                cr.Insert(comment);
                Comment temp = new Comment { Annonyms = comment.Annonyms, C_Id = comment.C_Id, ID = comment.ID, P_Id = comment.P_Id, rating = comment.rating, time = comment.time, Comment1 = comment.Comment1 };
                temp.links.Add(new Links() { Href = "http://localhost:8901/api/products/" + temp.P_Id + "/comments", method = "Get", rel = "All resource" });
                temp.links.Add(new Links() { Href = "http://localhost:8901/api/products/" + temp.P_Id + "/comments/" + temp.ID, method = "Get", rel = "specific Resource" });
                temp.links.Add(new Links() { Href = "http://localhost:8901/api/products/" + temp.P_Id + "/comments", method = "Post", rel = "Self" });
                temp.links.Add(new Links() { Href = "http://localhost:8901/api/products/" + temp.P_Id + "/comments/" + temp.ID, method = "Delete", rel = "Delete Resource" });
                temp.links.Add(new Links() { Href = "http://localhost:8901/api/products/" + temp.P_Id + "/comments/" + temp.ID, method = "put", rel = "Update Resource" });
                return Created(Url.Link("GetOneComment", new { cmid = temp.ID }), temp);
            }

        }
        [Route("{cmid}")]
        public IHttpActionResult Put([FromUri]int cmid, [FromBody]Comment cmnt)
        {
            var comment = cr.GetById(cmid.ToString());
            if (cmnt == null)
            {
                return StatusCode(HttpStatusCode.NoContent);
            }
            else
            {
                cmnt.ID = comment.ID;
                cr.Update(cmnt);
                cmnt.links.Add(new Links() { Href = "http://localhost:8901/api/products/" + cmnt.P_Id + "/comments", method = "Get", rel = "All Resource" });
                cmnt.links.Add(new Links() { Href = "http://localhost:8901/api/products/" + cmnt.P_Id + "/comments/" + cmnt.ID, method = "Get", rel = "specific Resource" });
                cmnt.links.Add(new Links() { Href = "http://localhost:8901/api/products/" + cmnt.P_Id + "/comments", method = "Post", rel = "Create Resource" });
                cmnt.links.Add(new Links() { Href = "http://localhost:8901/api/products/" + cmnt.P_Id + "/comments/" + cmnt.ID, method = "Delete", rel = "Delete Resource" });
                cmnt.links.Add(new Links() { Href = "http://localhost:8901/api/products/" + cmnt.P_Id + "/comments/" + cmnt.ID, method = "put", rel = "Self" });
                return Created(Url.Link("GetOneComment", new { cmid = cmnt.ID }), cmnt);
            }
        }
        [Route("{cmid}")]
        public IHttpActionResult Delete(int cmid)
        {
            var comment = cr.GetById(cmid.ToString());

            cr.Delete(comment);

            return StatusCode(HttpStatusCode.NoContent);
        }
        [Route("{cmid}/customers/{cid}")]
        public IHttpActionResult GetCommentCustomer(int cmid , string cid)
        {
            var comment = cr.GetById(cmid.ToString());
            if (comment == null)
            {
                return StatusCode(HttpStatusCode.NoContent);
            }
            else
            {
                var item = csr.GetById(comment.C_Id);
                Customer c = new Customer();
                c.Id = item.Id;
                c.Image = item.Image;
                c.Name = item.Name;
                c.PhoneNumber = item.PhoneNumber;
                c.Points = item.Points;
                c.Address = item.Address;
                c.Email = item.Email;
                //c.links.Add(new Links() { Href = "http://localhost:8901/api/customers", method = "Get", rel = "Self" });
                //c.links.Add(new Links() { Href = "http://localhost:8901/api/customers/" + c.Id, method = "Get", rel = "Specific Resource" });
                //c.links.Add(new Links() { Href = "http://localhost:8901/api/customers", method = "Post", rel = "Create Resource" });
                //c.links.Add(new Links() { Href = "http://localhost:8901/api/customers/" + c.Id, method = "Put", rel = "Update Resource" });
                //c.links.Add(new Links() { Href = "http://localhost:8901/api/customers/" + c.Id, method = "Delete", rel = "Delete Resource" });
                return Ok(c);
            }

        }
    }

}
