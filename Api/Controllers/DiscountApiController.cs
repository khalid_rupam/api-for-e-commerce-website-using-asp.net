﻿using Api.Attribute;
using Api.Models;
using Finalterm.Interface;
using Finalterm.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Api.Controllers
{
    [OrderCustomer]
    public class DiscountApiController : ApiController
    {
        FinalProjectEntities fp = new FinalProjectEntities();
        IRepository<Discount> arepo = new DiscountRepository(new FinalProjectEntities());
        //IRepository<Login> lrepo = new LoginRepository(new FinalProjectEntities());
        DiscountRepository dr = new DiscountRepository(new FinalProjectEntities());

        [Route("api/Discounts")]

        public IHttpActionResult Get()
        {
            var alist = arepo.GetAll().ToList();
            List<Discount> ad = new List<Discount>();
            foreach (var item in alist)
            {
                Discount d = new Discount();
                d.Id = item.Id;
                d.Discount1 = item.Discount1;


                d.links.Add(new Links() { Href = "http://localhost:8901/api/Discounts", method = "Get", rel = "Self" });
                d.links.Add(new Links() { Href = "http://localhost:8901/api/Discounts/ " + d.Id, method = "Get", rel = "Specific Resource" });
                d.links.Add(new Links() { Href = "http://localhost:8901/api/Discounts", method = "Post", rel = "Create Resource" });
                d.links.Add(new Links() { Href = "http://localhost:8901/api/Discounts/ " + d.Id, method = "Put", rel = "Update Resource" });
                d.links.Add(new Links() { Href = "http://localhost:8901/api/Discounts/ " + d.Id, method = "Delete", rel = "Delete Resource" });
                ad.Add(d);
            }
            return Ok(ad);

        }
        [Route("api/Discounts/{id}", Name = "GetBy")]
        public IHttpActionResult Get(string id)
        {
            var alist = arepo.GetAll().ToList().Where(p => p.Id == id).FirstOrDefault();

            if (alist == null)
            {
                return StatusCode(HttpStatusCode.NoContent);
            }
            else
            {
                Discount d = new Discount();
                d.Id = alist.Id;
                d.Discount1 = alist.Discount1;
                d.links.Add(new Links() { Href = "http://localhost:8901/api/Discounts", method = "Get", rel = "All Resources" });
                d.links.Add(new Links() { Href = "http://localhost:8901/api/Discounts/ " + d.Id, method = "Get", rel = "Self" });
                d.links.Add(new Links() { Href = "http://localhost:8901/api/Discounts", method = "Post", rel = "Create Resource" });
                d.links.Add(new Links() { Href = "http://localhost:8901/api/Discounts/ " + d.Id, method = "Put", rel = "Update Resource" });
                d.links.Add(new Links() { Href = "http://localhost:8901/api/Discounts/ " + d.Id, method = "Delete", rel = "Delete Resource" });
               
                return Ok(d);
            }
        }
        [Route("api/Discounts")]
        public IHttpActionResult Post(Discount d)
        {
            if (d == null)
            {
                return StatusCode(HttpStatusCode.NoContent);
            }
            else
            {
                fp.Discounts.Add(d);
                fp.SaveChanges();

                Discount d2 = new Discount();
                d2.Id = d.Id;
                d2.Discount1 = d.Discount1;
                d2.links.Add(new Links() { Href = "http://localhost:8901/api/Discounts", method = "Get", rel = "All Resource" });
                d2.links.Add(new Links() { Href = "http://localhost:8901/api/Discounts/ " + d2.Id, method = "Get", rel = "Specific Resource" });
                d2.links.Add(new Links() { Href = "http://localhost:8901/api/Discounts", method = "Post", rel = "Self" });
                d2.links.Add(new Links() { Href = "http://localhost:8901/api/Discounts/ " + d2.Id, method = "Put", rel = "Update Resource" });
                d2.links.Add(new Links() { Href = "http://localhost:8901/api/Discounts/ " + d2.Id, method = "Delete", rel = "Delete Resource" });

                return Created(Url.Link("GetBy", new { id = d2.Id }), d2);
            }
        }
        [Route("api/Discounts/{id}")]
        public IHttpActionResult Put([FromUri]string id, [FromBody]Discount ds)
        {
            var discount = arepo.GetById(id);

            if (discount == null)
            {
                return StatusCode(HttpStatusCode.NoContent);
            }
            else
            {
                ds.Id = discount.Id;
                ds.links.Add(new Links() { Href = "http://localhost:8901/api/Discounts", method = "Get", rel = "All Resource" });
                ds.links.Add(new Links() { Href = "http://localhost:8901/api/Discounts/ " + ds.Id, method = "Get", rel = "Specific Resource" });
                ds.links.Add(new Links() { Href = "http://localhost:8901/api/Discounts", method = "Post", rel = "Create Resource" });
                ds.links.Add(new Links() { Href = "http://localhost:8901/api/Discounts/ " + ds.Id, method = "Put", rel = "Self" });
                ds.links.Add(new Links() { Href = "http://localhost:8901/api/Discounts/ " + ds.Id, method = "Delete", rel = "Delete Resource" });
                dr.update(ds);
                return Created(Url.Link("GetIdBy", new { id = ds.Id }), ds);
            }

        }
        [Route("api/Discounts/{id}")]
        public IHttpActionResult Delete(string id)
        {
            var a = arepo.GetById(id);
            arepo.Delete(a);
            return StatusCode(HttpStatusCode.NoContent);
        }



    }

}
