﻿using Api.Attribute;
using Api.Models;
using Api.Repository;
using Finalterm.Interface;
using Finalterm.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Api.Controllers
{
    [RoutePrefix("api")]
    [OrderCustomer]
    public class CustomerOrderController : ApiController
    {
        IRepository<Login> lrepo = new LoginRepository(new FinalProjectEntities());
        IRepository<Order> Orepo = new OrderRepository(new FinalProjectEntities());
        IRepository<Product> prepo = new ProductRepostory(new FinalProjectEntities());
        IRepository<Payment> payrepo = new PaymentRepository(new FinalProjectEntities());
        IRepository<Sale> srepo = new SaleRepository(new FinalProjectEntities());
        IRepository<Customer> crepo = new CustomerRepository(new FinalProjectEntities());
        /// <summary>
        /// For specific Customer
        /// </summary>
        /// <returns></returns>
        [Route("Customers/{id}/Orders")]
        public IHttpActionResult Get(string id)
        {
            var a = Orepo.GetAll().ToList();
            List<Order> lo = new List<Order>();
            foreach (var OrderList in a)
            {
                Order o = new Order();
                o.C_Id = OrderList.C_Id;
                o.Address = OrderList.Address;
                o.ID = OrderList.ID;
                o.Pay_id = OrderList.Pay_id;
                o.P_ID = OrderList.P_ID;
                o.Quantity = OrderList.Quantity;
                o.Status = OrderList.Status;
                o.time = OrderList.time;
                o.links.Add(new Links() { Href = "http://localhost:8901/api/Customers/" + o.C_Id, method = "Get", rel = "Self" });
                o.links.Add(new Links() { Href = "http://localhost:8901/api/Customers/" + o.C_Id, method = "Get", rel = "Specific Resource" });
                //o.links.Add(new Links() { Href = "http://localhost:8901/api/Customers/"+o.C_Id + "/Orders", method = "Post", rel = "Create Resource" });
                o.links.Add(new Links() { Href = "http://localhost:8901/api/Customers/" + o.C_Id + "/Orders" + o.ID, method = "Put", rel = "Update Resource" });
                o.links.Add(new Links() { Href = "http://localhost:8901/api/Customers/" + o.C_Id + "/Orders", method = "Delete", rel = "Delete resource" });
                lo.Add(o);
            }
            return Ok(lo);
            
        }
        [Route("Customers/{id}/Orders/{Oid}",Name = "GetoneOrder")]
        public IHttpActionResult Get(string id, int Oid)
        {
            var OrderList = Orepo.GetAll().ToList().Where(p => p.C_Id == id).Where(q => q.ID == Oid).FirstOrDefault();

            if (OrderList == null)
            {
                return StatusCode(HttpStatusCode.NoContent);
            }
            else
            {
                Order o = new Order();
                o.C_Id = OrderList.C_Id;
                o.Address = OrderList.Address;
                o.ID = OrderList.ID;
                o.Pay_id = OrderList.Pay_id;
                o.P_ID = OrderList.P_ID;
                o.Quantity = OrderList.Quantity;
                o.Sales = OrderList.Sales;
                o.Status = OrderList.Status;
                o.time = OrderList.time;
                o.links.Add(new Links() { Href = "http://localhost:8901/api/Customers/" + o.C_Id, method = "Get", rel = "All Resource" });
                o.links.Add(new Links() { Href = "http://localhost:8901/api/Customers/" + o.C_Id, method = "Get", rel = "Self" });
                o.links.Add(new Links() { Href = "http://localhost:8901/api/Customers/" + o.C_Id + "/Orders", method = "Post", rel = "Create Resource" });
                o.links.Add(new Links() { Href = "http://localhost:8901/api/Customers/" + o.C_Id + "/Orders" + o.ID, method = "Put", rel = "Update Resource" });
                o.links.Add(new Links() { Href = "http://localhost:8901/api/Customers/" + o.C_Id + "/Orders" + o.ID, method = "Delete", rel = "Delete resource" });

                return Ok(o);
            }
        }
        [Route("Customers/{id}/Orders/{Oid}")]
        public IHttpActionResult Put([FromUri]string id, [FromUri]int Oid, [FromBody]Order o)
        {
            var order = Orepo.GetAll().ToList().Where(p => p.C_Id == id).Where(q => q.ID == Oid).FirstOrDefault();
            OrderRepository or = new OrderRepository(new FinalProjectEntities());
            var creee = crepo.GetById(id);
            if (order == null)
            {
                return StatusCode(HttpStatusCode.NoContent);
            }
            else
            {
                int i = o.Status;
                string s = DateTime.Now.ToString();
                o = order;
                o.Status = i;
                o.time = s;
                or.update(o);

                Order o1 = new Order();
                o1.Address = o.Address;
                o1.C_Id = o.C_Id;
                o1.ID = o.ID;
                o1.Pay_id = o.Pay_id;
                o1.P_ID = o.P_ID;
                o1.Quantity = o.Quantity;
                o1.Sales = o.Sales;
                o1.Status = o.Status;
                o1.time = o.time;
                o1.links.Add(new Links() { Href = "http://localhost:8901/api/Customers/" + o.C_Id, method = "Get", rel = "All Resource" });
                o1.links.Add(new Links() { Href = "http://localhost:8901/api/Customers/" + o.C_Id, method = "Get", rel = "Specific Resources" });
                o1.links.Add(new Links() { Href = "http://localhost:8901/api/Customers/" + o.C_Id + "/Orders", method = "Post", rel = "Create Resource" });
                o1.links.Add(new Links() { Href = "http://localhost:8901/api/Customers/" + o.C_Id + "/Orders" + o.ID, method = "Put", rel = "Self" });
                o1.links.Add(new Links() { Href = "http://localhost:8901/api/Customers/" + o.C_Id + "/Orders" + o.ID, method = "Delete", rel = "Delete resource" });
                return Ok(o1);
            }
        }
        [Route("Customers/{id}/Orders")]
        public IHttpActionResult Post([FromUri]string id, [FromBody]Order o)
        {
            var valid= lrepo.GetById(id);
            dynamic product;
            try
            {
                product = prepo.GetById(o.P_ID.ToString());
            }
            catch (Exception)
            {
                return StatusCode(HttpStatusCode.NoContent);
            }
            
            if(o==null&&valid==null)
            {
                return StatusCode(HttpStatusCode.NoContent);
            }
            else
            {
                Payment p = new Payment();
                Random r = new Random();
                p.Id = r.Next(9999);
                p.Method = "1";
                p.Status = 0;
                p.Amount = product.Sell_Price;
                payrepo.Insert(p);
                Order o1 = new Order();
                o1.C_Id = valid.ID;
                o1.Pay_id = p.Id;
                o1.P_ID = product.Id;
                o1.Quantity = o.Quantity;
                o1.Address = o.Address;
                o1.Status = 0;
                o1.time = DateTime.Now.ToString();
                Orepo.Insert(o1);
                o1.links.Add(new Links() { Href = "http://localhost:8901/api/Customers/" +id, method = "Get", rel = "All Resource" });
                o1.links.Add(new Links() { Href = "http://localhost:8901/api/Customers/" + id, method = "Get", rel = "Specific Resources" });
                o1.links.Add(new Links() { Href = "http://localhost:8901/api/Customers/" + id + "/Orders", method = "Post", rel = "Create Resource" });
                o1.links.Add(new Links() { Href = "http://localhost:8901/api/Customers/" + id + "/Orders" + o1.ID, method = "Put", rel = "Self" });
                o1.links.Add(new Links() { Href = "http://localhost:8901/api/Customers/" + id + "/Orders" + o1.ID, method = "Delete", rel = "Delete resource" });

                Sale s = new Sale();
                s.O_Id = o1.ID;
                s.profit = product.Sell_Price - product.Buy_Price;
                srepo.Insert(s);
                return Created(Url.Link("GetoneOrder", new { id = id, Oid = o1.ID }), o1);
            }
        }
        [Route("Customers/{id}/Orders/{Oid}")]
        public IHttpActionResult Delete(string id, int Oid)
        {
            var order = Orepo.GetAll().ToList().Where(p => p.C_Id == id).Where(q => q.ID == Oid).FirstOrDefault();
            if (order == null)
            {
                return StatusCode(HttpStatusCode.NoContent);
            }
            else
            {
                Orepo.Delete(order);
                return StatusCode(HttpStatusCode.NoContent);
            }

        }
        [Route("Customers/{id}/Orders/{Oid}/Products")]
        public IHttpActionResult GetProducts(string id,string Oid)
        {
            Product p = new Product();
            var order = Orepo.GetAll().Where(p1 => p1.ID == int.Parse(Oid)).FirstOrDefault();
            var product = prepo.GetAll().ToList().Where(p1 => p1.Id == order.P_ID).FirstOrDefault();
            if(product==null)
            {
                return StatusCode(HttpStatusCode.NoContent);
            }
            else
            {
                p.Brand = product.Brand;
                p.Sell_Price = product.Sell_Price;
                p.Catagory_Id = product.Catagory_Id;
                p.Description = product.Description;
                p.Id = product.Id;
                p.image = product.image;
                p.Name = product.Name;
                p.Rating = product.Rating;
                p.Unit_Id = product.Unit_Id;
                return Ok(p);
            }
        }
        [Route("Customers/{id}/Orders/{Oid}/Payments")]
        public IHttpActionResult Get(string id, string Oid)
        {
            Payment p = new Payment();
            var order = Orepo.GetAll().Where(p1 => p1.ID == int.Parse(Oid)).FirstOrDefault();
            var payment = payrepo.GetAll().ToList().Where(p1 => p1.Id == order.Pay_id).FirstOrDefault();
            if (payment == null)
            {
                return StatusCode(HttpStatusCode.NoContent);
            }
            else
            {
                p.Id = payment.Id;
                p.Method = payment.Method;
                p.Status = payment.Status;
                p.Amount = payment.Amount;
                
                return Ok(p);
            }
        }
    }
}
