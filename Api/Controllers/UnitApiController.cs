﻿using Api.Attribute;
using Api.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Api.Controllers
{
    [OrderCustomer]
    public class UnitApiController : ApiController
    {
        FinalProjectEntities fp = new FinalProjectEntities();
        [Route("api/Units")]
        public IHttpActionResult Get()
        {
            var l = fp.Units.ToList();
            List<Unit> ut = new List<Unit>();

            foreach (var item in l)
            {
                Unit u = new Unit();
                u.Id = item.Id;
                u.Name= item.Name;
                u.links.Add(new Links() { Href = "http://localhost:8901/api/Units", method = "Get", rel = "Self" });
                u.links.Add(new Links() { Href = "http://localhost:8901/api/Units/" + u.Id, method = "Get", rel = "Specific Resource" });
                u.links.Add(new Links() { Href = "http://localhost:8901/api/Units", method = "Post", rel = "Create Resource" });
                u.links.Add(new Links() { Href = "http://localhost:8901/api/Units/" + u.Id, method = "Put", rel = "Update Resource" });
                u.links.Add(new Links() { Href = "http://localhost:8901/api/Units/" + u.Id, method = "Delete", rel = "Delete Resource" });
                ut.Add(u);
            }
            return Ok(ut);
        }
        [Route("api/Units/{id}", Name = "GetId")]
        public IHttpActionResult Get(int id)
        {
            var l = fp.Units.Where(p => p.Id == id).FirstOrDefault();

            if (l == null)
            {
                return StatusCode(HttpStatusCode.NoContent);
            }
            else
            {

                Unit u = new Unit();
                u.Id = l.Id;
                u.Name = l.Name;
                u.links.Add(new Links() { Href = "http://localhost:8901/api/Units", method = "Get", rel = "All Resources" });
                u.links.Add(new Links() { Href = "http://localhost:8901/api/Units/" + u.Id, method = "Get", rel = "Self" });
                u.links.Add(new Links() { Href = "http://localhost:8901/api/Units", method = "Post", rel = "Create Resource" });
                u.links.Add(new Links() { Href = "http://localhost:8901/api/Units/" + u.Id, method = "Put", rel = "Update Resource" });
                u.links.Add(new Links() { Href = "http://localhost:8901/api/Units/" + u.Id, method = "Delete", rel = "Delete Resource" });
                return Ok(u);
            }
           
        }
        [Route("api/Units")]
        public IHttpActionResult Post(Unit u)
        {
            if (u == null)
            {
                return StatusCode(HttpStatusCode.NoContent);
            }
            else
            {
                fp.Units.Add(u);
                fp.SaveChanges();

                Unit u2 = new Unit();
                u2.Id = u.Id;
                u2.Name = u.Name;
                u2.links.Add(new Links() { Href = "http://localhost:8901/api/Units", method = "Get", rel = "All Resources" });
                u2.links.Add(new Links() { Href = "http://localhost:8901/api/Units/" + u2.Id, method = "Get", rel = "Specific Resource" });
                u2.links.Add(new Links() { Href = "http://localhost:8901/api/Units", method = "Post", rel = "Self" });
                u2.links.Add(new Links() { Href = "http://localhost:8901/api/Units/" + u2.Id, method = "Put", rel = "Update Resource" });
                u2.links.Add(new Links() { Href = "http://localhost:8901/api/Units/" + u2.Id, method = "Delete", rel = "Delete Resource" });

                return Created(Url.Link("GetId", new { id = u2.Id }), u2);
            }
        }
        [Route("api/Units/{id}")]
        public IHttpActionResult Put([FromUri]int id, [FromBody]Unit u)
        {
            var l = fp .Units.Where(p1 => p1.Id == id).FirstOrDefault();

            if (l == null)
            {
                return StatusCode(HttpStatusCode.NoContent);
            }
            else
            {
                l.Id = id;
                l.Name = u.Name;
                fp.SaveChanges();
                Unit u2 = new Unit();
                u2.Id = u.Id;
                u2.Name = u.Name;
                u2.links.Add(new Links() { Href = "http://localhost:8901/api/Units", method = "Get", rel = "All Resources" });
                u2.links.Add(new Links() { Href = "http://localhost:8901/api/Units/" + u2.Id, method = "Get", rel = "Specific Resource" });
                u2.links.Add(new Links() { Href = "http://localhost:8901/api/Units", method = "Post", rel = "Create Resource" });
                u2.links.Add(new Links() { Href = "http://localhost:8901/api/Units/" + u2.Id, method = "Put", rel = "Self" });
                u2.links.Add(new Links() { Href = "http://localhost:8901/api/Units/" + u2.Id, method = "Delete", rel = "Delete Resource" });
                return Ok(u2);
            }

        }

        [Route("api/Units/{id}")]
        public IHttpActionResult Delete(int id)
        {
            var l = fp.Units.Where(p1 => p1.Id == id).FirstOrDefault();

            if (l == null)
            {
                return StatusCode(HttpStatusCode.NoContent);
            }
            else
            {
                fp.Units.Remove(l);
                fp.SaveChanges();
                return StatusCode(HttpStatusCode.NoContent);
            }
        }
    }

}