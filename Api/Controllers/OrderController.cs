﻿using Api.Attribute;
using Api.Models;
using Finalterm.Interface;
using Finalterm.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Api.Controllers
{
    [RoutePrefix("api/Admins")]
    [OrderCustomer]
    public class OrderController : ApiController
    {
        IRepository<Order> Orepo = new OrderRepository(new FinalProjectEntities());
        /// <summary>
        /// For Admins
        /// </summary>
        /// <returns></returns>
        [Route("Orders")]
        public IHttpActionResult Get()
        {
            var OrderList = Orepo.GetAll().ToList();

            List<Order> lo = new List<Order>();

            foreach (var item in OrderList)
            {
                Order o = new Order();
                o.C_Id = item.C_Id;
                o.Address = item.Address;
                o.ID = item.ID;
                o.Pay_id = item.Pay_id;
                o.P_ID = item.P_ID;
                o.Quantity = item.Quantity;
                o.Sales = item.Sales;
                o.Status = item.Status;
                o.time = item.time;
                o.Customer = null;
                o.Payment = null;
                o.Product = null;
               
                
                o.links.Add(new Links() { Href = "http://localhost:8901/api/Admins/orders", method = "Get", rel = "Self" });
                o.links.Add(new Links() { Href = "http://localhost:8901/api/Admins/orders/" + o.ID, method = "Get", rel = "Specific Resource" });
                o.links.Add(new Links() { Href = "http://localhost:8901/api/Admins/orders/" + o.ID, method = "Put", rel = "Update Resource" });
                o.links.Add(new Links() { Href = "http://localhost:8901/api/Admins/orders/" + o.ID, method = "Delete", rel = "Delete resource" });
                lo.Add(o);
            }
            return Ok(lo);
        }
        [Route("Orders/{id}")]
        public IHttpActionResult Get(int id)
        {
            var OrderList = Orepo.GetAll().ToList().Where(p=>p.ID==id).FirstOrDefault();
            

            if (OrderList==null)
            {
                return StatusCode(HttpStatusCode.NoContent);
            }
            else
            {
                Order o = new Order();
                o.C_Id = OrderList.C_Id;
                o.Address = OrderList.Address;
                o.ID = OrderList.ID;
                o.Pay_id = OrderList.Pay_id;
                o.P_ID = OrderList.P_ID;
                o.Quantity = OrderList.Quantity;
                o.Sales = OrderList.Sales;
                o.Status = OrderList.Status;
                o.time = OrderList.time;
                o.links.Add(new Links() { Href = "http://localhost:8901/api/Admins/orders", method = "Get", rel = "All resources" });
                o.links.Add(new Links() { Href = "http://localhost:8901/api/Admins/orders/" + o.ID, method = "Get", rel = "Self" });
                o.links.Add(new Links() { Href = "http://localhost:8901/api/Admins/orders/" + o.ID, method = "Put", rel = "Update Resource" });
                o.links.Add(new Links() { Href = "http://localhost:8901/api/Admins/orders/" + o.ID, method = "Delete", rel = "Delete resource" });

                return Ok(o);
            }

        }
        [Route("Orders/{id}")]
        public IHttpActionResult Put(int id,Order o)
        {
            var order = Orepo.GetAll().Where(p => p.ID == id).FirstOrDefault();
            OrderRepository or = new OrderRepository(new FinalProjectEntities());
            if (order==null)
            {
                return StatusCode(HttpStatusCode.NoContent);
            }
            else
            {
                int i = o.Status;
                string s = DateTime.Now.ToString();
                o = order;
                o.Status = i;
                o.time = s;
                or.update(o);

                Order o1 = new Order();
                o1.Address = o.Address;
                o1.C_Id = o.C_Id;
                o1.ID = o.ID;
                o1.Pay_id = o.Pay_id;
                o1.P_ID = o.P_ID;
                o1.Quantity = o.Quantity;
                o1.Sales = o.Sales;
                o1.Status = o.Status;
                o1.time = o.time;
                
                return Ok(o1);
            }
        }
        [Route("Orders/{id}")]
        public IHttpActionResult Delete(int id)
        {
            var order = Orepo.GetById(id.ToString());
            if(order==null)
            {
                return StatusCode(HttpStatusCode.NoContent);
            }
            else
            {
                Orepo.Delete(order);
                return StatusCode(HttpStatusCode.NoContent);
            }
            
        }


        /// <summary>
        /// For specific Customer
        /// </summary>
        /// <returns></returns>
        [Route("Customers/{id}/Orders")]
        public IHttpActionResult Get(string id)
        {
            var OrderList = Orepo.GetAll().ToList().Where(p => p.C_Id == id).FirstOrDefault();
            
            if (OrderList == null)
            {
                return StatusCode(HttpStatusCode.NoContent);
            }
            else
            {
                Order o = new Order();
                o.C_Id = OrderList.C_Id;
                o.Address = OrderList.Address;
                o.ID = OrderList.ID;
                o.Pay_id = OrderList.Pay_id;
                o.P_ID = OrderList.P_ID;
                o.Quantity = OrderList.Quantity;
                o.Sales = OrderList.Sales;
                o.Status = OrderList.Status;
                o.time = OrderList.time;
                o.links.Add(new Links() { Href = "http://localhost:8901/api/Admins/Customers/" + o.C_Id, method = "Get", rel = "Self" });
                o.links.Add(new Links() { Href = "http://localhost:8901/api/Admins/Customers/" + o.C_Id , method = "Get", rel = "Specific Resource" });
                //o.links.Add(new Links() { Href = "http://localhost:8901/api/Admins/Customers/"+o.C_Id + "/Orders", method = "Post", rel = "Create Resource" });
                o.links.Add(new Links() { Href = "http://localhost:8901/api/Admins/Customers/" + o.C_Id + "/Orders" + o.ID, method = "Put", rel = "Update Resource" });
                o.links.Add(new Links() { Href = "http://localhost:8901/api/Admins/Customers/" + o.C_Id + "/Orders", method = "Delete", rel = "Delete resource" });

                return Ok(o);
            }
        }
        [Route("Customers/{id}/Orders/{Oid}")]
        public IHttpActionResult Get(string id, int Oid)
        {
            var OrderList = Orepo.GetAll().ToList().Where(p => p.C_Id == id).Where(q => q.ID == Oid).FirstOrDefault();

            if (OrderList == null)
            {
                return StatusCode(HttpStatusCode.NoContent);
            }
            else
            {
                Order o = new Order();
                o.C_Id = OrderList.C_Id;
                o.Address = OrderList.Address;
                o.ID = OrderList.ID;
                o.Pay_id = OrderList.Pay_id;
                o.P_ID = OrderList.P_ID;
                o.Quantity = OrderList.Quantity;
                o.Sales = OrderList.Sales;
                o.Status = OrderList.Status;
                o.time = OrderList.time;
                o.links.Add(new Links() { Href = "http://localhost:8901/api/Admins/Customers/" + o.C_Id, method = "Get", rel = "All Resource" });
                o.links.Add(new Links() { Href = "http://localhost:8901/api/Admins/Customers/" + o.C_Id, method = "Get", rel = "Self" });
                //o.links.Add(new Links() { Href = "http://localhost:8901/api/Admins/Customers/" + o.C_Id + "/Orders", method = "Post", rel = "Create Resource" });
                o.links.Add(new Links() { Href = "http://localhost:8901/api/Admins/Customers/" + o.C_Id + "/Orders" + o.ID, method = "Put", rel = "Update Resource" });
                o.links.Add(new Links() { Href = "http://localhost:8901/api/Admins/Customers/" + o.C_Id + "/Orders" + o.ID, method = "Delete", rel = "Delete resource" });

                return Ok(o);
            }

        }
        [Route("Customers/{id}/Orders/{Oid}")]
        public IHttpActionResult Put(string id,int Oid,Order o)
        {
            var order = Orepo.GetAll().ToList().Where(p => p.C_Id == id).Where(q => q.ID == Oid).FirstOrDefault();
            OrderRepository or = new OrderRepository(new FinalProjectEntities());
            if (order == null)
            {
                return StatusCode(HttpStatusCode.NoContent);
            }
            else
            {
                int i = o.Status;
                string s = DateTime.Now.ToString();
                o = order;
                o.Status = i;
                o.time = s;
                or.update(o);

                Order o1 = new Order();
                o1.Address = o.Address;
                o1.C_Id = o.C_Id;
                o1.ID = o.ID;
                o1.Pay_id = o.Pay_id;
                o1.P_ID = o.P_ID;
                o1.Quantity = o.Quantity;
                o1.Sales = o.Sales;
                o1.Status = o.Status;
                o1.time = o.time;
                o1.links.Add(new Links() { Href = "http://localhost:8901/api/Admins/Customers/" + o.C_Id, method = "Get", rel = "All Resource" });
                o1.links.Add(new Links() { Href = "http://localhost:8901/api/Admins/Customers/" + o.C_Id, method = "Get", rel = "Specific Resources" });
                //o.links.Add(new Links() { Href = "http://localhost:8901/api/Admins/Customers/" + o.C_Id + "/Orders", method = "Post", rel = "Create Resource" });
                o1.links.Add(new Links() { Href = "http://localhost:8901/api/Admins/Customers/" + o.C_Id + "/Orders" + o.ID, method = "Put", rel = "Self" });
                o1.links.Add(new Links() { Href = "http://localhost:8901/api/Admins/Customers/" + o.C_Id + "/Orders" + o.ID, method = "Delete", rel = "Delete resource" });
                return Ok(o1);
            }
        }
        [Route("Customers/{id}/Orders/{Oid}")]
        public IHttpActionResult Delete(string id,int Oid)
        {
            var order = Orepo.GetAll().ToList().Where(p => p.C_Id == id).Where(q => q.ID == Oid).FirstOrDefault();
            if (order == null)
            {
                return StatusCode(HttpStatusCode.NoContent);
            }
            else
            {
                Orepo.Delete(order);
                return StatusCode(HttpStatusCode.NoContent);
            }

        }
    }
}
