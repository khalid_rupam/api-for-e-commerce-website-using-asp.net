﻿using Api.Attribute;
using Api.Models;
using Finalterm.Interface;
using Finalterm.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Api.Controllers
{
    [OrderCustomer]
    public class AdminApiController : ApiController
    {
        FinalProjectEntities fp = new FinalProjectEntities();
        IRepository<Admin> arepo = new AdminRepository(new FinalProjectEntities());
        IRepository<Login> lrepo = new LoginRepository(new FinalProjectEntities());
        AdminRepository ar = new AdminRepository(new FinalProjectEntities());

        [Route("api/AdminsInfos")]
        public IHttpActionResult Get()
        {
            var alist = arepo.GetAll().ToList();
            List<Admin> ad = new List<Admin>();
            foreach (var item in alist)
            {
                Admin a = new Admin();
                a.Id = item.Id;
                a.Name = item.Name;
                a.Address = item.Address;
                a.Email = item.Email;
                a.PhoneNumber = item.PhoneNumber;
                a.Salary = item.Salary;
               
                a.links.Add(new Links() { Href = "http://localhost:8901/api/Admins", method = "Get", rel = "Self" });
                a.links.Add(new Links() { Href = "http://localhost:8901/api/Admins/" + a.Id, method = "Get", rel = "Specific Resource" });
                a.links.Add(new Links() { Href = "http://localhost:8901/api/Admins", method = "Post", rel = "Create Resource" });
                a.links.Add(new Links() { Href = "http://localhost:8901/api/Admins/" + a.Id, method = "Put", rel = "Update Resource" });
                a.links.Add(new Links() { Href = "http://localhost:8901/api/Admins/" + a.Id, method = "Delete", rel = "Delete Resource" });
                ad.Add(a);
            }
            return Ok(ad);

        }
        [Route("api/AdminsInfos/{id}", Name = "GetByIdadmin")]
        public IHttpActionResult Get(string id)
        {
            var alist = arepo.GetAll().ToList().Where(p => p.Id == id).FirstOrDefault();

            if (alist == null)
            {
                return StatusCode(HttpStatusCode.NoContent);
            }
            else
            {
                Admin a = new Admin();
                a.Id =alist.Id;
                a.Name = alist.Name;
                a.Address = alist.Address;
                a.Email = alist.Email;
                a.PhoneNumber = alist.PhoneNumber;
                a.Salary = alist.Salary;

                a.links.Add(new Links() { Href = "http://localhost:8901/api/Admins", method = "Get", rel = "All Resources" });
                a.links.Add(new Links() { Href = "http://localhost:8901/api/Admins/" + a.Id, method = "Get", rel = "Self" });
                a.links.Add(new Links() { Href = "http://localhost:8901/api/Admins", method = "Post", rel = "Create Resource" });
                a.links.Add(new Links() { Href = "http://localhost:8901/api/Admins/" + a.Id, method = "Put", rel = "Update Resource" });
                a.links.Add(new Links() { Href = "http://localhost:8901/api/Admins/" + a.Id, method = "Delete", rel = "Delete Resource" });
                return Ok(a);
            }
        }

        [Route("api/AdminsInfos")]
        public IHttpActionResult Post(Admin a)
        {
            if (a == null)
            {
                return StatusCode(HttpStatusCode.NoContent);
            }
            else
            {
                Random r = new Random();
                int ran = r.Next(999);
                Login l = new Login();
                string s = "E" + ran;
                l.ID = s;
                l.Password = r.Next(99999).ToString();
                l.Status = 0;
                l.Online = 0;
                l.Type = 0;
                lrepo.Insert(l);
                a.Id = s;

                a.links.Add(new Links() { Href = "http://localhost:8901/api/Admins", method = "Get", rel = "All resources" });
                a.links.Add(new Links() { Href = "http://localhost:8901/api/Admins/" + a.Id, method = "Get", rel = "Specific Resource" });
                a.links.Add(new Links() { Href = "http://localhost:8901/api/Admins", method = "Post", rel = "Create Resource" });
                a.links.Add(new Links() { Href = "http://localhost:8901/api/Admins/" + a.Id, method = "Put", rel = "Update Resource" });
                a.links.Add(new Links() { Href = "http://localhost:8901/api/Admins/" + a.Id, method = "Delete", rel = "Delete Resource" });
                arepo.Insert(a);
                return Created(Url.Link("GetByIdadmin", new { id = a.Id }), a);
            }
        }
        
        [Route("api/AdminsInfos/{id}")]
        public IHttpActionResult Put([FromUri]string id, [FromBody]Admin ad)
        {
            var admin = arepo.GetById(id);

            if (admin == null)
            {
                return StatusCode(HttpStatusCode.NoContent);
            }
            else
            {
                ad.Id = admin.Id;

                ad.links.Add(new Links() { Href = "http://localhost:8901/api/Admins", method = "Get", rel = "Self" });
                ad.links.Add(new Links() { Href = "http://localhost:8901/api/Admins/" + ad.Id, method = "Get", rel = "Specific Resource" });
                ad.links.Add(new Links() { Href = "http://localhost:8901/api/Admins", method = "Post", rel = "Create Resource" });
                ad.links.Add(new Links() { Href = "http://localhost:8901/api/Admins/" + ad.Id, method = "Put", rel = "Update Resource" });
                ad.links.Add(new Links() { Href = "http://localhost:8901/api/Admins/" + ad.Id, method = "Delete", rel = "Delete Resource" });
                ar.update(ad);
                return Created(Url.Link("GetByIdadmin", new { id = ad.Id }), ad);
            }

        }
        [Route("api/AdminsInfos/{id}")]
        public IHttpActionResult Delete(string id)
        {
            var a = arepo.GetById(id);
            arepo.Delete(a);
            return StatusCode(HttpStatusCode.NoContent);
        }
    }
}
