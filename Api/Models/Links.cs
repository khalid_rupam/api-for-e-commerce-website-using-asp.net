﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Api.Models
{
    public class Links
    {
        public string Href { get; set; }
        public string method { get; set; }
        public string rel { get; set; }
    }
}