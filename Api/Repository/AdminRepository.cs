﻿using Api.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Finalterm.Repository
{
    public class AdminRepository:Repository<Admin>
    {
        FinalProjectEntities f = new FinalProjectEntities();
        public AdminRepository(FinalProjectEntities fr):base(fr)
        {

        }
        public void update(Admin a)
        {
            Admin a1 = f.Admins.Find(a.Id);
            // Login l1 = f.Logins.Find(a.Id);
            a1.Id = a.Id;
            a1.Name = a.Name;
            a1.PhoneNumber = a.PhoneNumber;
            a1.Email = a.Email;
            a1.Address = a.Address;
            a1.Salary = a.Salary;
            // l1.Status = s;
            //c1.Points = c1.Points;
            f.SaveChanges();
        }
    }
}