﻿using Api.Models;
using Finalterm.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Api.Repository
{
    public class UnitRepository:Repository<Unit>
    {
        public UnitRepository(FinalProjectEntities fr):base(fr)
        {
                
        }
    }
}