﻿using Api.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Finalterm.Repository
{
    public class CommentRepository:Repository<Comment>
    {
        FinalProjectEntities fr = new FinalProjectEntities();
        public CommentRepository(FinalProjectEntities fr):base(fr)
        {

        }
        public List<Comment> GetByProduct(int productID)
        {
            return fr.Comments.Where(x => x.P_Id == productID).ToList();
        }
        public void Update(Comment c)
        {
            Comment C1 = fr.Comments.Find(c.ID);
            C1.ID = c.ID;
            C1.P_Id = c.P_Id;
            C1.rating = c.rating;
            C1.time = DateTime.Now.ToString();
            C1.Annonyms = c.Annonyms;
            C1.Comment1 = c.Comment1;
            C1.C_Id = c.C_Id;
            fr.SaveChanges();
        }
    }
}