﻿using Api.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Finalterm.Repository
{
    public class ProductRepostory:Repository<Product>
    {
        FinalProjectEntities fr = new FinalProjectEntities();
        public ProductRepostory(FinalProjectEntities fr):base(fr)
        {
        }
        public void update(Product p)
        {
            Product p1 = fr.Products.Find(p.Id);
            p1.Name = p.Name;
            p1.Quantity = p.Quantity;
            p1.Unit_Id = p.Unit_Id;
            p1.Catagory_Id = p.Catagory_Id;
            p1.Buy_Price = p.Buy_Price;
            p1.Brand = p.Brand;
            p1.Sell_Price = p.Sell_Price;
            fr.SaveChanges();
        }
        public List<Product> GetByString(string search)
        {
            List<Product> temp = fr.Products.Where(x => x.Name.Contains(search) || x.Catagory.Name.Contains(search) || x.Brand.Contains(search) || search == null).ToList();
            return temp;
        }
        public List<Product> GetByBrand(string search)
        {
            List<Product> temp = fr.Products.Where(x => x.Brand.Contains(search) || search == null).ToList();
            return temp;
        }
        public List<Product> GetBySort(string sort, string search)
        {
            List<Product> temp = fr.Products.Where(x => x.Name.Contains(search) || search == null).ToList();
            if (sort == "high")
            {
                return temp.OrderByDescending(x => x.Sell_Price).ToList();

            }
            else
            {
                return temp.OrderBy(x => x.Sell_Price).ToList();
            }

        }
        public double GetPrice(double id)
        {
            return fr.Products.Find(id).Sell_Price;
        }

        //internal object GetByString(IPagedList<char> pagedList)
        //{
        //    throw new NotImplementedException();
        //}
        public List<Product> GetByCategory(int? search)
        {
            //List<Product> temp = fr.Products.Where(x => x.Catagory_Id.Equals(search)).ToList();
            List<Product> temp = fr.Products.ToList();
            return temp;
        }
    }
}