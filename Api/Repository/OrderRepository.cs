﻿using Api.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Finalterm.Repository
{
    public class OrderRepository:Repository<Order>
    {
        FinalProjectEntities f = new FinalProjectEntities();
        public OrderRepository(FinalProjectEntities fr):base(fr)
        {

        }
        public void update(Order o)
        {
            Order o1 = f.Orders.Find(o.ID);
            o1.Status = o.Status;
            o1.time = o.time;
            f.SaveChanges();
        }
    }
}