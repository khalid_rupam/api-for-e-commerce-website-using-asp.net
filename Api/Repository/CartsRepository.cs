﻿using Api.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Finalterm.Repository
{
    public class CartsRepository: Repository<Cart>
    {
        private FinalProjectEntities fr = new FinalProjectEntities();
        public CartsRepository(FinalProjectEntities fr) : base(fr)
        {

        }
        
        public List<Cart> GetByIdc(string id)
        {
            return fr.Carts.Where(x => x.C_ID.Contains(id)).ToList();
        }
        public void update(Cart a)
        {
            Cart c = new Cart();
            c.C_ID = a.C_ID;
            c.Id = a.Id;
            c.Price = a.Price;
            c.P_Id = a.P_Id;
            c.Quantity = a.Quantity;
            c.time = a.time;
            fr.SaveChanges();
        }
    }
}