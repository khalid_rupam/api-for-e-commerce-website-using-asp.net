﻿using Api.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Finalterm.Repository
{
    public class CatagoryRepository:Repository<Catagory>
    {
        FinalProjectEntities f = new FinalProjectEntities();
        public CatagoryRepository(FinalProjectEntities fr):base(fr)
        {
        }
        public void update(Catagory cr)
        {
            Catagory c = f.Catagories.Find(cr.Id);
            // Login l1 = f.Logins.Find(a.Id);
            c.Id = cr.Id;
            c.Name = cr.Name;
            // l1.Status = s;
            //c1.Points = c1.Points;
            f.SaveChanges();
        }
    }
}