﻿using Finalterm.Interface;
using Api.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Finalterm.Repository
{
    public class DiscountRepository:Repository<Discount>
    {
        FinalProjectEntities f = new FinalProjectEntities();
        public DiscountRepository(FinalProjectEntities fr):base(fr)
        {

        }
        public void update(Discount dr)
        {
            Discount d = f.Discounts.Find(dr.Id);
            // Login l1 = f.Logins.Find(a.Id);
            d.Id = dr.Id;
            d.Discount1 = dr.Discount1;
            f.SaveChanges();
        }
    }
}