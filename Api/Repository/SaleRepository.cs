﻿using Api.Models;
using Finalterm.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Api.Repository
{
    public class SaleRepository:Repository<Sale>
    {
        public SaleRepository(FinalProjectEntities fr):base(fr)
        {

        }
    }
}