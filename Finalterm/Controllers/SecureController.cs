﻿using Finalterm.Interface;
using Finalterm.Models;
using Finalterm.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Finalterm.Controllers
{
    public class SecureController : Controller
    {
        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            string l = Session["Login"] as string;
            if (string.IsNullOrEmpty(l))
            {
                filterContext.Result = new RedirectToRouteResult(
                   new System.Web.Routing.RouteValueDictionary { { "controller", "Home" }, { "action", "Index" } });
                return;
            }
        }
    }
}