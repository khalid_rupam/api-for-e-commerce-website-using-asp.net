﻿using Finalterm.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PagedList.Mvc;
using PagedList;
using Finalterm.Models;

namespace Finalterm.Controllers
{
    public class ProductsController : Controller
    {
        ProductRepostory repo = new ProductRepostory(new Models.FinalProjectEntities());
        //GET: ProSearch
        public ActionResult Search(string searchVal, string brandsVal,string sort, int? page)
        {
           Session["search"] = searchVal;
            if(sort !=null && searchVal!=null)
            {
                return View(repo.GetBySort(sort,searchVal).ToPagedList(page ?? 1, 5));
            }
            if (brandsVal!=null)
            {
                return View(repo.GetByBrand(brandsVal).ToPagedList(page ?? 1, 5));
            }
           else
            {
                return View(repo.GetByString(searchVal).ToPagedList(page ?? 1, 5));
            }
            
        }
        public ActionResult Details(string id)
        {
            return View(repo.GetById(id));
        }
        public ActionResult Search1(string searchVal, string brandsVal, string sort, int? categoryVal, int? page)
        {
            Session["search"] = searchVal;
            if (sort != null && searchVal != null)
            {
                return View(repo.GetBySort(sort, searchVal).ToPagedList(page ?? 1, 5));
            }
            if (brandsVal != null)
            {
                return View(repo.GetByBrand(brandsVal).ToPagedList(page ?? 1, 5));
            }
            if (categoryVal != null)
            {
                return View(repo.GetByCategory(categoryVal).ToPagedList(page ?? 1, 5));
            }
            else
            {
                return View(repo.GetByString(searchVal).ToPagedList(page ?? 1, 5));
            }

        }
    }
}