﻿using Finalterm.Models;
using Finalterm.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Finalterm.Controllers
{
    public class CartsController : SecureController
    {
        CartsRepository repo = new CartsRepository(new Models.FinalProjectEntities());
        ProductRepostory productrepo = new ProductRepostory(new Models.FinalProjectEntities());
        
        public ActionResult Index()
        {
            List<Cart> temp = repo.GetByIdc(Session["Login"].ToString());
            double total = 0;
            foreach(var item in temp)
            {
                total += item.Price;
            }
            Session["total"] = total;
            string s = Session["Login"].ToString();
            return View(repo.GetByIdc(s));
        }
        public ActionResult AddToCart(int productID,int quantity)
        {
            Cart temp = new Cart();
            temp.C_ID = Session["Login"].ToString();
            temp.P_Id = productID;
            temp.Quantity = quantity;
            temp.Price = productrepo.GetPrice(Convert.ToDouble(productID)) * quantity;
            temp.time = DateTime.Now.ToShortTimeString();
            repo.Insert(temp);

            return RedirectToAction("Index");
        }
        public ActionResult Delete(string id)
        {
           repo.Delete(repo.GetById(id));
            return RedirectToAction("Index");
        }
    }
}