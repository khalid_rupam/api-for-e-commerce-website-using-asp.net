﻿using Finalterm.Interface;
using Finalterm.Models;
using Finalterm.Repository;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;
using System.Web.Mvc;

namespace Finalterm.Controllers
{
    public class HomeController : Controller
    {
        IRepository<Product> prepo = new ProductRepostory(new FinalProjectEntities());
        IRepository<frentite> frrepo = new FrontRepository(new FinalProjectEntities());
        IRepository<Customer> crrepo = new CustomerRepository(new FinalProjectEntities());
        IRepository<Order> orrepo = new OrderRepository(new FinalProjectEntities());
        IRepository<Login> lrepo = new LoginRepository(new FinalProjectEntities());
        IRepository<Product> papo = new ProductRepostory(new FinalProjectEntities());
        IRepository<Catagory> catra = new CatagoryRepository(new FinalProjectEntities());
        IRepository<Comment> capo = new CommentRepository(new FinalProjectEntities());

        public ActionResult start()
        {
            Session["Login"] = 0;
            Session["Id"] = 0;
            Session["Coupon"] = 0;
            return RedirectToAction("Index");
        }
        //public ActionResult start()
        //{
        //    Session["Id"] = 1;
        //    Product p = new Product();
        //    return View(papo.GetAll().ToList());
        //}
        [HttpGet]
        public ActionResult Index()
        {

            Product p = new Product();
            List<frentite> lf= frrepo.GetAll().ToList().OrderBy(p1=>p1.Id).ToList();
            int count = 0;
            foreach (var item in lf)
            {
                if(count==0)
                {
                    ViewBag.zero = item.productId;
                    ViewBag.one = item.time.ToString();
                }
                else if(count==1)
                {
                    ViewBag.three = item.productId;
                    ViewBag.two = item.time.ToString();
                }
                else if (count == 2)
                {
                    ViewBag.four = item.productId;
                    ViewBag.five = item.time.ToString();
                }
                count += 1;
            }
            List<Catagory> cat = catra.GetAll().ToList();
            ViewBag.Cata = cat;
            ViewBag.Pata = papo.GetAll().ToList();
            return View(papo.GetAll().ToList());
        }
        [HttpPost]
        public ActionResult Index(string name)
        {
            return Content(name);
        }
        [HttpGet]
        public ActionResult Details(int id)
        {
            //var pid = id;
            Session["pid"] = id;
            Session["Coupon"] = 0;
            List<Comment> cm = new List<Comment>();

            List<Comment> lc = capo.GetAll().ToList();
            foreach (var item in lc)
            {
                if (item.P_Id == id)
                {
                    Comment cmd = new Comment();
                    cmd.ID = item.ID;
                    cmd.Product = item.Product;
                    cmd.P_Id = item.P_Id;
                    cmd.rating = item.rating;
                    cmd.time = item.time;
                    cmd.Comment1 = item.Comment1;
                    cmd.Annonyms = item.Annonyms;
                    cmd.Customer = item.Customer;
                    cm.Add(cmd);
                }
            }
            ViewBag.cus = cm;
            double count = 0.0;
            double rat = 0.0;
            foreach (var item in lc)
            {


                if (item.P_Id == id)
                {
                    count += item.rating;
                    rat += 1;
                }
            }
            double d = count / rat;
            ViewBag.avgrating = d;
            return View(papo.GetAll());
            //return Content("");


        }
        [HttpPost]
        public ActionResult Details()
        {
            Comment c = new Comment();
            //var pid = id;
            // Session["pid"] = id;
            return View(papo.GetAll());
            // return View();


        }
        public ActionResult Logout()
        {
            Login l = lrepo.GetById(Session["Login"].ToString());
            LoginRepository lr = new LoginRepository(new FinalProjectEntities());
            l.Online = 0;
            lr.update(l);
            Session["Login"] = 0;
            Session["Id"] = 0;
            return RedirectToAction("Index");

        }
        [HttpGet]
        public ActionResult Login()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Login(Login l)
        {
            Login l1= lrepo.GetById(l.ID);
            if (l1==null)
            {
                return View("Login");
            }
            else
            {
                if (l1.ID==l.ID&&l1.Password==l.Password)
                {
                    if(l1.Status==0&&l1.Type==0)
                    {
                        Session["Login"] = l1.ID;
                        Session["Id"] = "1";
                        Login l5 = new Login();
                        l5 = l1;
                        l5.Online = 1;
                        LoginRepository l3 = new LoginRepository(new FinalProjectEntities());
                        l3.update(l5);
                        return RedirectToAction("Index");
                    }
                    else if(l1.Status == 0 && l1.Type == 1)
                    {
                        Session["Login"] = l1.ID;
                        Session["Id"] = "1";
                        return RedirectToAction("Admin");
                    }
                }
                else
                {
                    return View("Index");
                }
                return View("Index");
            }

        }
        public ActionResult ShopNow(string name)
        {
            return Content(name);
        }
        public ActionResult Admin()
        {
            AdminDashboard ad = new AdminDashboard();
            ad.NoOfOrders = orrepo.GetAll().ToList().Count();


            int i = lrepo.GetAll().Where(p => p.Online == 1).Count();

            ad.Online = i;

            List<Order> ol = orrepo.GetAll().ToList();
            List<Order> fl = new List<Order>();
            ad.NoOfSales = 0;
            ad.Jan = 0;
            ad.feb = 0;
            ad.march = 0;
            ad.april = 0;
            ad.may = 0;
            ad.jun = 0;
            ad.july = 0;
            ad.Aug = 0;
            ad.Sept = 0;
            ad.Oct = 0;
            ad.Nov = 0;
            ad.Dec = 0;
            string s= DateTime.Now.ToString();
            foreach (var item in ol)
            {
                if(item.Status==1)
                {
                    ad.NoOfSales += 1;
                }
                else
                {
                    fl.Add(item);
                }
            }
            foreach (var item in ol)
            {
                if(item.time.ToString().Substring(0, 2)=="1/")
                {
                    ad.Jan += 1;
                }
                else if (item.time.ToString().Substring(0, 2) == "2/")
                {
                    ad.feb += 1;
                }
                else if (item.time.ToString().Substring(0, 2) == "3/")
                {
                    ad.march += 1;
                }
                else if (item.time.ToString().Substring(0, 2) == "4/")
                {
                    ad.april += 1;
                }
                else if (item.time.ToString().Substring(0, 2) == "5/")
                {
                    ad.may += 1;
                }
                else if (item.time.ToString().Substring(0, 2) == "6/")
                {
                    ad.jun += 1;
                }
                else if (item.time.ToString().Substring(0, 2) == "7/")
                {
                    ad.july += 1;
                }
                else if (item.time.ToString().Substring(0, 2) == "8/")
                {
                    ad.Aug += 1;
                }
                else if (item.time.ToString().Substring(0, 2) == "9/")
                {
                    ad.Sept += 1;
                }
                else if (item.time.ToString().Substring(0, 2) == "10")
                {
                    ad.Oct += 1;
                }
                else if (item.time.ToString().Substring(0, 2) == "11")
                {
                    ad.Nov += 1;
                }
                else if (item.time.ToString().Substring(0, 2) == "12")
                {
                    ad.Dec += 1;
                }
            }
            ad.NoOfCustomer = crrepo.GetAll().ToList().Count();
            ad.orders = fl;
            return View(ad);
        }
        public ActionResult Discount()
        {
            return Content("I am here");    
        }
        public ActionResult AddComment(int quantity, string anno, string comment)
        {
            Comment c = new Comment();
            c.Annonyms = int.Parse(anno);
            c.P_Id = int.Parse(Session["pid"].ToString());
            c.time = DateTime.Now.ToString();
            c.rating = quantity;
            c.C_Id = Session["Login"].ToString();
            c.Comment1 = comment;
            capo.Insert(c);
            return RedirectToAction("Index");
        }
        public ActionResult ViewComment(Comment com)
        {
            return View();

        }
        public ActionResult SignUp()
        {
            return View();
        }
        [HttpPost]
        public ActionResult SignUp(Customer c, HttpPostedFileBase image)
        {
            if (ModelState.IsValid)
            {
                Login l = new Login();

                Random r = new Random();
                l.ID = c.Id;
                l.Password = r.Next().ToString();
                l.Type = 0;
                l.Status = 0;
                l.Online = 0;
                lrepo.Insert(l);

                string path = "-1";
                int random = r.Next();
                if (image != null && image.ContentLength > 0)
                {
                    string extension = Path.GetExtension(image.FileName);
                    if (extension.ToLower().Equals(".jpg") || extension.ToLower().Equals(".png"))
                    {
                        try
                        {
                            path = Path.Combine(Server.MapPath("~/Image/"), random + Path.GetFileName(image.FileName));
                            image.SaveAs(path);
                            path = "~/Image/" + random + Path.GetFileName(image.FileName);
                        }
                        catch (Exception)
                        {
                            return Content("Upload fail catch");
                        }
                    }
                    else
                    {
                        return Content("Uplad  else");
                    }
                }

                try
                {
                    SmtpClient clientDetais = new SmtpClient();
                    clientDetais.Port = 587;
                    clientDetais.Host = "smtp.gmail.com";
                    clientDetais.EnableSsl = true;
                    clientDetais.DeliveryMethod = SmtpDeliveryMethod.Network;
                    clientDetais.UseDefaultCredentials = false;
                    clientDetais.Credentials = new NetworkCredential("librarymanagementSystemAIUB@gmail.com", "khalidhasan.khr");

                    MailMessage maildetails = new MailMessage();
                    maildetails.From = new MailAddress("librarymanagementSystemAIUB@gmail.com");
                    maildetails.To.Add(c.Email);
                    maildetails.Subject = "Account Created";
                    maildetails.Body = "Welcome to BuyHere " + c.Name + "\n Your customer Id:" + c.Id + "AND YOUR PASSWORD IS " + l.Password;
                    clientDetais.Send(maildetails);
                    c.Image = path;
                    crrepo.Insert(c);
                    return RedirectToAction("Index");
                }
                catch (Exception)
                {

                    return RedirectToAction("Index"); 
                }
               
            }
            return View();
        }
        public ActionResult Addcus()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Addcus(Customer c, HttpPostedFileBase image)
        {
            if (ModelState.IsValid)
            {
                Login l = new Login();

                Random r = new Random();
                l.ID = c.Id;
                l.Password = r.Next().ToString();
                l.Type = 0;
                l.Status = 0;
                l.Online = 0;
                lrepo.Insert(l);

                string path = "-1";
                int random = r.Next();
                if (image != null && image.ContentLength > 0)
                {
                    string extension = Path.GetExtension(image.FileName);
                    if (extension.ToLower().Equals(".jpg") || extension.ToLower().Equals(".png"))
                    {
                        try
                        {
                            path = Path.Combine(Server.MapPath("~/Image/"), random + Path.GetFileName(image.FileName));
                            image.SaveAs(path);
                            path = "~/Image/" + random + Path.GetFileName(image.FileName);
                        }
                        catch (Exception)
                        {
                            return Content("Upload fail catch");
                        }
                    }
                    else
                    {
                        return Content("Uplad  else");
                    }
                }

                SmtpClient clientDetais = new SmtpClient();
                clientDetais.Port = 587;
                clientDetais.Host = "smtp.gmail.com";
                clientDetais.EnableSsl = true;
                clientDetais.DeliveryMethod = SmtpDeliveryMethod.Network;
                clientDetais.UseDefaultCredentials = false;
                clientDetais.Credentials = new NetworkCredential("librarymanagementSystemAIUB@gmail.com", "khalidhasan.khr");

                MailMessage maildetails = new MailMessage();
                maildetails.From = new MailAddress("librarymanagementSystemAIUB@gmail.com");
                maildetails.To.Add(c.Email);
                maildetails.Subject = "Account Created";
                maildetails.Body = "Welcome to BuyHere " + c.Name + "\n Your customer Id:" + c.Id + "AND YOUR PASSWORD IS " + l.Password;
                clientDetais.Send(maildetails);
                c.Image = path;
                crrepo.Insert(c);
                return RedirectToAction("Admin");
            }
            return View();
            
        }
        public ActionResult ViewOrder()
        {
            return View(orrepo.GetAll().ToList().Where(p => p.C_Id == Session["Login"].ToString()));
        }
        [HttpGet]
        public ActionResult MyProfile()
        {
            return View(crrepo.GetById(Session["Login"].ToString()));
        }
        [HttpPost]
        public ActionResult MyProfile(Customer c)
        {
            if(ModelState.IsValid)
            {

                CustomerRepository cr = new CustomerRepository(new FinalProjectEntities());
                cr.update(c, 0);

                return View(crrepo.GetById(Session["Login"].ToString()));
            }
            else
            {
                return View();
            }
        }
    }
}