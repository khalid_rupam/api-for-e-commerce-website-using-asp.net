﻿using Finalterm.Interface;
using Finalterm.Models;
using Finalterm.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Finalterm.Controllers
{
    public class MessageController : SecureController

    {
        IRepository<Message> repo = new MessageRepository(new FinalProjectEntities());
        // GET: Message
        [HttpGet]
        public ActionResult Index()
        {

            return View();
        }
        [HttpPost]
        public ActionResult Index(Message mgs)
        {

            Message m = new Message();
            m.Sender_Id = mgs.Sender_Id;
            m.Messages = mgs.Messages;
            m.Status = 0;
            

            repo.Insert(m);
            return RedirectToAction("Index");

            //return RedirectToAction("Index","Message","<script>alert('Message Sent.Check Your Registered Email address or wait for reply.thank you');</script>");
        }
    }
}