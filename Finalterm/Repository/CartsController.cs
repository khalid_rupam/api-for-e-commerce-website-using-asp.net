﻿using Finalterm.Models;
using Finalterm.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Finalterm.Controllers
{
    public class CartsController : Controller
    {
        CartsRepository repo = new CartsRepository(new Models.FinalProjectEntities());
        ProductRepository productrepo = new ProductRepository(new Models.FinalProjectEntities());
        
        public ActionResult Index()
        {
            List<Cart> temp = repo.GetById(Session["userId"].ToString());
            double total = 0;
            foreach(var item in temp)
            {
                total += item.Price;
            }
            Session["total"] = total;
            return View(repo.GetById(Session["userId"].ToString()));
        }
        public ActionResult AddToCart(int productID,int quantity)
        {
            Cart temp = new Cart();
            temp.C_ID = Session["userId"].ToString();
            temp.P_Id = productID;
            temp.Quantity = quantity;
            temp.Price = productrepo.GetPrice(Convert.ToDouble(productID)) * quantity;
            temp.time = DateTime.Now.ToShortTimeString();
            repo.Insert(temp);

            return RedirectToAction("Index");
        }
        public ActionResult Delete(int id)
        {
            repo.Delete(repo.GetById(id));
            return RedirectToAction("Index");
        }
    }
}